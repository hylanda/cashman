package com.suncorp.listeners;

import com.suncorp.model.Tender;

/**
 * Listeners that called during transaction events such as a successful withdrawal
 */
public interface TransactionListener  {

    void onTransaction(Tender tender);
}
