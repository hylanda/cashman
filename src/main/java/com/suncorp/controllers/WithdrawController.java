package com.suncorp.controllers;

import com.suncorp.model.Transaction;
import com.suncorp.services.impl.DefaultTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Serve withdraw screen and output initial ATM amount - for testing purposes only
 */
@Controller
public class WithdrawController {

    @Autowired
    DefaultTransactionService transactionService;


    @RequestMapping("/")
    public String withdraw(@RequestParam(value="amount", required=false) Integer amount, Model model) {
        if ( amount != null) {
            if (amount > 0) {
                Transaction transaction = transactionService.withdraw(amount);
                if ( transaction.getErrorMessage() != null)
                    model.addAttribute("message", transaction.getErrorMessage());
                else
                    model.addAttribute("message", "Withdrew: " + transaction.getTender().toString());

            }
        } else {
            model.addAttribute("message", "Available tender in ATM - " + transactionService.getAvailableTender().toString());
        }
        return "withdraw";
    }

}
