package com.suncorp.services;

import com.suncorp.model.Tender;

public interface TransactionService {

    /**
     * Withdraw cash, updated amount in ATM and return the number of notes/coins dispensed
     * @param amount withdrawl amount
     * @return A transaction object specifying the quantity of each notes/coin dispensed for the required amount
     * @throws IllegalArgumentException If amount entered is not greater than or equal to zero
     */
    void withdraw (int amount);


    /**
     * Return available tender from ATM
     * @return Tender available tender
     */
    Tender getAvailableTender();
}
