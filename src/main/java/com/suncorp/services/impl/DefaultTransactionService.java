package com.suncorp.services.impl;

import com.suncorp.daos.TenderDao;
import com.suncorp.listeners.TransactionListener;
import com.suncorp.model.Denomination;
import com.suncorp.model.Tender;
import com.suncorp.model.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Business logic for withdrawing cash
 */
@Service
public class DefaultTransactionService {

    private List<TransactionListener> listeners;

    private TenderDao tenderDao;

    public DefaultTransactionService() {
        listeners = new ArrayList<>();
    }

    @Autowired
    public void setTenderDao(TenderDao tenderDao) {
        this.tenderDao = tenderDao;
    }

    public Tender getAvailableTender() {
        return tenderDao.getAvailableTender();
    }

    public synchronized Transaction withdraw (int amount) {

        if (amount < 0)
            throw new IllegalArgumentException("Amount not in valid range");

        Tender availableTender = getAvailableTender();
        Transaction transaction = new Transaction();

        if (amount > availableTender.getCalculatedAmount()) {
            transaction.setErrorMessage("Not enough tender available for requested amount");
            return transaction;
        }

        // calculate the combinations of tender to return back
        // based on what's available
        Tender tender = calculateSolution(new Tender(), Denomination.getLargetAmount(), amount);

        if (tender == null) {
            transaction.setErrorMessage("Could not dispense tender for amount specified");
            return transaction;
        }

        transaction.setTender(tender);

        tender.asMap().entrySet().forEach(entry-> {
            availableTender.reduceDenomination(entry.getKey(), entry.getValue());
        });

        tenderDao.update(availableTender);

        listeners.forEach(listener -> listener.onTransaction(new Tender(tender)));

        return transaction;
    }

    public void registerListener(TransactionListener listener) {
        listeners.add(listener);
    }

    public void deregisterListener(TransactionListener listener) {
        listeners.remove(listener);
    }

    private Tender calculateSolution(Tender tender, Denomination denomination, int amount) {
        // iterate through each denomination removing it from the requested amount until 0 is achieved or return null.
        // Favour a solution dispensing higher denominations that don't deplete any one denomination
        int denominationValue = denomination.getDenominationValue();

        if (amount >= denominationValue) {
            int tenderQuantity = getAvailableTender().getDenominationQuantity(denomination);
            int quantity = Math.min(amount / denominationValue, tenderQuantity);

            if (tenderQuantity - quantity == 0 && denomination.nextLargest() != null) {
                // depletes this tender prefer alternate solution
                Tender alternateResult = calculateSolution(tender, denomination.nextLargest(), amount);
                if (alternateResult != null)
                    return alternateResult;
            }

            if (amount - denominationValue * quantity == 0) {
                tender.setDenominationQuantity(denomination, quantity);
                return tender;
            } else if (denomination.nextLargest() != null) {

                Tender result = calculateSolution(tender, denomination.nextLargest(), amount - denominationValue * quantity);

                if (result != null) {
                    result.setDenominationQuantity(denomination, quantity);
                    return result;
                }

                // retry tender combination that  doesn't use the current denomination
                if (denomination.nextLargest() != null)
                    return calculateSolution(tender, denomination.nextLargest(), amount);
            } else
                return null;
        } else if (denomination.nextLargest() != null)
            return calculateSolution(tender, denomination.nextLargest(), amount);

        return null;
    }


}

