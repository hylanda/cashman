package com.suncorp.model;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.stream.IntStream;

/**
 * Represents the quantity held for each denomination
 */
public class Tender {

    private Map<Denomination, Integer> tenderMap;

    public Tender() {
        tenderMap = new HashMap<>();
    }

    public Tender(Tender tender) {
        tenderMap = tender.asMap();
    }

    /**
     * Set number of notes or coins for a denomination
     * @param denomination
     * @param quantity
     */
    public void setDenominationQuantity(Denomination denomination, int quantity) {
        tenderMap.put(denomination, quantity);
    }

    /**
     * Return the number of notes or coins for a denomination
     * @param denomination
     * @return quantity of denomination
     */
    public int getDenominationQuantity(Denomination denomination) {
        if (tenderMap.get(denomination) != null)
            return tenderMap.get(denomination);

        return 0;
    }

    /**
     * Return dollar amount
     * @return dollar amount
     */
    public int getCalculatedAmount() {
        return tenderMap.entrySet().stream().map( (entry) -> entry.getKey().getDenominationValue() * entry.getValue()).flatMapToInt(IntStream::of).sum();
    }

    /**
     * Reduce the quantity of a denomination
     * @param denomination
     * @param quantity
     * @throws IllegalArgumentException if quantity is greater than available amount
     */
    public void reduceDenomination(Denomination denomination, int quantity) {
        int currentQuality = getDenominationQuantity(denomination);
        if ( quantity > currentQuality )
            throw new IllegalArgumentException("Requested quantity " + quantity + " is less than available amount " + currentQuality);

        int newQuantity = currentQuality - quantity;
        if (newQuantity == 0)
            tenderMap.remove(denomination);
        else
            tenderMap.put(denomination, currentQuality - quantity);
    }

    public Map<Denomination,Integer> asMap() {
        return new HashMap<>(tenderMap);
    }

    @Override
    public String toString() {
        StringBuffer buffer =new StringBuffer();

        Iterator<Map.Entry<Denomination, Integer>> it = tenderMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<Denomination, Integer> entry = it.next();
            buffer.append("$" + entry.getKey().getDenominationValue() + " x " + entry.getValue());
            if ( it.hasNext())
                buffer.append(", ");
        }

        return buffer.toString();
    }

}
