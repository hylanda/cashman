package com.suncorp.model;

/**
 * POJO/DTO for a cash transaction
 */
public class Transaction {

    private Tender tender;
    private String errorMessage;

    public Transaction() {}

    public Transaction(Tender tender) {
        this.tender = tender;
    }

    public Transaction(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Tender getTender() {
        return tender;
    }

    public void setTender(Tender tender) {
        this.tender = tender;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
