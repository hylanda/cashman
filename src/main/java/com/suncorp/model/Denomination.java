package com.suncorp.model;

/**
 * Represents different denominations of legal tender
 */
public enum Denomination {
    TWENTY("TWENTY", 20),
    FIFTY("FIFTY", 50);

    private String name;
    private int denominationValue;

    Denomination(String name, int denominationValue) {
        this.name = name;
        this.denominationValue = denominationValue;
    }

    /**
     * Get numerical value of denomination
     * @return numerical value
     */
    public int getDenominationValue() {
        return denominationValue;
    }

    /**
     * Return largest denomination available
     * @return largest denomination
     */
    public static Denomination getLargetAmount() {
        return FIFTY;
    }

    /**
     * Return next largest denomination
     * @return Next largest denomination or null if there is no other
     */
    public Denomination nextLargest() {
        switch(this) {
            case FIFTY:
                return TWENTY;
            case TWENTY:
                return null;
        }

        return null;
    }

}
