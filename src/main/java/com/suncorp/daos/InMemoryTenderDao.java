package com.suncorp.daos;

import com.suncorp.model.Denomination;
import com.suncorp.model.Tender;
import org.springframework.stereotype.Component;

/**
 * In memory implementation of dao for available funds in ATM
 */
@Component
public class InMemoryTenderDao implements TenderDao {

    private Tender tender;

    public InMemoryTenderDao() {
        tender = new Tender();
        tender.setDenominationQuantity(Denomination.FIFTY, 3);
        tender.setDenominationQuantity(Denomination.TWENTY, 8);
    }

    public Tender getAvailableTender() {
        return tender;
    }

    public void update(Tender tender) {
        this.tender = tender;
    }

}
