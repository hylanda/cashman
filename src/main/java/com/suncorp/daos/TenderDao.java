package com.suncorp.daos;

import com.suncorp.model.Tender;

/**
 * Represents interface to data source that contains ATM tender information
 */
public interface TenderDao {

    /**
     * Retrieve available tender
     * @return Available tender for ATM
     */
    Tender getAvailableTender();

    /**
     * Update the available tender
     * @param Tender new Tender about
     */
    void update(Tender tender);
}
