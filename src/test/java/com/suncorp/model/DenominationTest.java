package com.suncorp.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class DenominationTest {

    @Test
    public void testLargest() {
        assertEquals(Denomination.getLargetAmount(), Denomination.FIFTY);
    }

    @Test
    public void testNextLargest() {
        assertEquals(Denomination.FIFTY.nextLargest(), Denomination.TWENTY);
        assertNull(Denomination.TWENTY.nextLargest());
    }
}
