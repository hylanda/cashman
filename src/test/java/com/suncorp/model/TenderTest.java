package com.suncorp.model;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class TenderTest {

    private Tender tender;

    @Before
    public void beforeEach() {
        tender = new Tender();
    }

    //TODO: parameterized

    @Test
    public void testGetCalculatedAmount () {
        tender.setDenominationQuantity(Denomination.TWENTY, 5);
        tender.setDenominationQuantity(Denomination.FIFTY, 4);
        assertEquals(tender.getCalculatedAmount(), 300);

        tender = new Tender();
        assertEquals(tender.getCalculatedAmount(), 0 );
    }

    @Test
    public void testReduceDenomation() {
        tender.setDenominationQuantity(Denomination.TWENTY, 1);
        tender.reduceDenomination(Denomination.TWENTY, 1);
        assertEquals(tender.getDenominationQuantity(Denomination.TWENTY), 0);

        try {
            tender.reduceDenomination(Denomination.TWENTY,1);
            fail();
        } catch (IllegalArgumentException ie) {
            // success
        }
    }

    @Test
    public void testAsMap() {
        tender.setDenominationQuantity(Denomination.TWENTY, 20);
        tender.setDenominationQuantity(Denomination.FIFTY, 50);
        Map<Denomination, Integer> map = tender.asMap();
        assertNotNull(map.get(Denomination.TWENTY));
        assertNotNull(map.get(Denomination.FIFTY));
        assertTrue(map.get(Denomination.TWENTY) == 20);
        assertTrue(map.get(Denomination.FIFTY) == 50);
    }
}
