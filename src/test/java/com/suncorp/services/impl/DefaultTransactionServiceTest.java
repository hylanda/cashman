package com.suncorp.services.impl;

import com.suncorp.daos.InMemoryTenderDao;
import com.suncorp.model.Denomination;
import com.suncorp.model.Tender;
import com.suncorp.model.Transaction;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class DefaultTransactionServiceTest {

    private DefaultTransactionService service;
    private InMemoryTenderDao mockedDao;

    @Before
    public void before() {
        service = new DefaultTransactionService();

        mockedDao = mock(InMemoryTenderDao.class);
        Tender tender = new Tender();

        tender.setDenominationQuantity(Denomination.TWENTY, 8);
        tender.setDenominationQuantity(Denomination.FIFTY, 3);
        when(mockedDao.getAvailableTender()).thenReturn(tender);

        service.setTenderDao(mockedDao);
    }

    private Transaction transaction(int fifties, int twenties, String err ) {
        Transaction transaction = new Transaction();
        transaction.setErrorMessage(err);

        Tender tender = new Tender();
        tender.setDenominationQuantity(Denomination.TWENTY, twenties);
        tender.setDenominationQuantity(Denomination.FIFTY, fifties);

        transaction.setTender(tender);
        return  transaction;
    }

    private Transaction transaction(int fifties, int twenties) {
        return transaction(fifties, twenties, null);
    }

    @Test
    public void testWithdraw() {

        int[] inputs = new int[]{20, 40, 50, 70, 80, 100, 150, 60};
        Transaction[] results = new Transaction[]{
                transaction(0, 1),
                transaction(0, 2),
                transaction(1, 0),
                transaction(1, 1),
                transaction(0, 4),
                transaction(2, 0),
                transaction(3, 0),
                transaction(0, 3)};

        for (int i = 0; i < inputs.length; i++) {
            before();

            Transaction transaction = service.withdraw(inputs[i]);
            assertEquals(transaction.getTender().getDenominationQuantity(Denomination.TWENTY), results[i].getTender().getDenominationQuantity(Denomination.TWENTY));
            assertEquals(transaction.getTender().getDenominationQuantity(Denomination.FIFTY), results[i].getTender().getDenominationQuantity(Denomination.FIFTY));
            assertEquals(transaction.getErrorMessage(), results[i].getErrorMessage());
            verify(mockedDao).update(any());
        }

        assertEquals(service.withdraw(110).getErrorMessage(), "Could not dispense tender for amount specified");
        assertEquals(service.withdraw(200).getErrorMessage(), "Could not dispense tender for amount specified");

        try {
            service.withdraw(-10);
            fail();
        } catch (IllegalArgumentException ie) {
            // success
        }
    }

}
